using System.Net;

namespace iti_products.Models
{
    public class DefaultError
    {
        public HttpStatusCode code { get; set; }
        public string message { get; set; }
    }
}