using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iti_products.Models
{
    public class Product
    {
        public string id { get; set; }

        [Key]
        [Required]
        public string sku { get; set; }

        [Required]
        public string name { get; set; }

        [Required]
        public string shortDescription { get; set; }

        [Required]
        public string longDescription { get; set; }

        [Required]
        public string imageUrl { get; set; }

        [Required]
        public virtual Price price { get; set; }

    }
}