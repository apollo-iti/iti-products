using iti_products.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iti_products.Maps
{
    public class ProductMap
    {
        public ProductMap(EntityTypeBuilder<Product> entityBuilder)
        {
            entityBuilder.ToTable("product");
            entityBuilder.HasKey(x => x.id);

            entityBuilder.Property(x => x.sku).HasColumnName("sku");
            entityBuilder.Property(x => x.name).HasColumnName("name");
            entityBuilder.Property(x => x.shortDescription).HasColumnName("shortDescription");
            entityBuilder.Property(x => x.longDescription).HasColumnName("longDescription");
            entityBuilder.Property(x => x.imageUrl).HasColumnName("imageUrl");
            // entityBuilder.OwnsOne(x => x.price,
            //         price =>
            //         {
            //             price.Property(x => x.amount).HasColumnName("amount");
            //             price.Property(x => x.scale).HasColumnName("scale");
            //             price.Property(x => x.currencyCode).HasColumnName("currency_code");
            //         }).HasNoKey();
        }
    }
}