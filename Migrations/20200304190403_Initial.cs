﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace iti_products.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    sku = table.Column<string>(nullable: false),
                    id = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: false),
                    shortDescription = table.Column<string>(nullable: false),
                    longDescription = table.Column<string>(nullable: false),
                    imageUrl = table.Column<string>(nullable: false),
                    amount = table.Column<int>(nullable: true),
                    scale = table.Column<int>(nullable: true),
                    currency_code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.sku);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Products");
        }
    }
}
