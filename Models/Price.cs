using System.ComponentModel.DataAnnotations.Schema;

namespace iti_products.Models
{
    [ComplexType]
    public class Price
    {
        [Column("amount")]
        public int amount { get; set; }

        [Column("scale")]
        public int scale { get; set; }

        [Column("currency_code")]
        public string currencyCode { get; set; }
    }
}