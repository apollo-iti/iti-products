using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using iti_products.Models;
using System.Text.Json;

namespace iti_products.Controllers
{
    [ApiController]
    [Route("products")]
    public class ProductController : ControllerBase
    {
        private readonly ILogger<ProductController> _logger;
        private readonly DatabaseContext _database;

        public ProductController(ILogger<ProductController> logger, DatabaseContext context)
        {
            _logger = logger;
            _database = context;
        }

        [HttpGet]
        [Route("list")]
        public List<Product> ListProducts() => _database.listProducts();

        [HttpGet]
        [Route("{sku}")]
        public IActionResult GetProduct(string sku)
        {
            try
            {
                return Ok(_database.getProduct(sku));
            }
            catch
            {
                return NotFound(new DefaultError()
                {
                    code = HttpStatusCode.NotFound,
                    message = $"Can't find product with sku {sku}"
                });
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult AddProduct([FromBody] Product product)
        {
            _logger.LogInformation("Add product: " + JsonSerializer.Serialize(product));
            try
            {
                product.imageUrl = "https://marvel-live.freetls.fastly.net/canvas/2020/2/1974477c97a54aeca692c1df411d8771?quality=95&fake=.png";
                _database.AddProduct(product);
                return Created("", product);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException ex)
            {
                return Conflict(new DefaultError()
                {
                    code = HttpStatusCode.Conflict,
                    message = $"SKU {product.sku} already exists"
                });
            }
        }

        [HttpDelete]
        [Route("delete/{sku}")]
        [AllowAnonymous]
        public object DeleteProduct(string sku)
        {
            try
            {
                var product = _database.getProduct(sku);
                _logger.LogInformation("Delete product for sku: {sku}", sku);
                _database.DeleteProduct(product.sku);
                return Ok(product);
            }
            catch
            {
                return NotFound(new DefaultError()
                {
                    code = HttpStatusCode.NotFound,
                    message = $"Can't find product with sku {sku}"
                });
            }
        }


        [HttpPut]
        [Route("update")]
        [AllowAnonymous]
        public object UpdateProduct([FromBody] Product product)
        {
            try
            {
                _logger.LogInformation("Update product for sku: {sku}", product.sku);
                _database.UpdateProduct(product);
                return Ok(product);
            }
            catch
            {
                return NotFound(new DefaultError()
                {
                    code = HttpStatusCode.NotFound,
                    message = $"Can't find product with sku {product.sku}"
                });
            }
        }
    }
}