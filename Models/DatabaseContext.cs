using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using iti_products.Maps;

namespace iti_products.Models
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Product> Products { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            ApplyMigrations(this);
        }

        public void ApplyMigrations(DatabaseContext context)
        {
            if (context.Database.GetPendingMigrations().Any())
            {
                context.Database.Migrate();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Product>(
                entity =>
                {
                    entity.HasKey(x => x.sku);
                    entity.Property(x => x.id).HasColumnName("id");
                    entity.Property(x => x.name).HasColumnName("name");
                    entity.Property(x => x.shortDescription).HasColumnName("shortDescription");
                    entity.Property(x => x.longDescription).HasColumnName("longDescription");
                    entity.Property(x => x.imageUrl).HasColumnName("imageUrl");
                    entity.OwnsOne(
                        x => x.price,
                            p =>
                            {
                                p.Property(p => p.amount).HasColumnName("amount");
                                p.Property(p => p.scale).HasColumnName("scale");
                                p.Property(p => p.currencyCode).HasColumnName("currency_code");
                            });
                }
            );

        }

        public List<Product> listProducts() => Products.OrderBy(x => x.sku).ToList<Product>();

        public Product getProduct(string sku) => Products.Where(x => x.sku == sku).First();

        public void AddProduct(Product product)
        {
            product.id = Guid.NewGuid().ToString();
            Products.Add(product);
            this.SaveChanges();
        }

        public void DeleteProduct(string sku)
        {
            Products.Remove(Products.Where(x => x.sku == sku).First());
            this.SaveChanges();
        }

        public void UpdateProduct(Product product)
        {
            Products.Update(product);
            this.SaveChanges();
        }
    }
}